jQuery(document).ready(function () {
    jQuery('.single-item').slick({
        dots: true,
        mobileFirst: true,
        arrows: false,
        customPaging: function (slider, i) {
            return $('<button /><span />').text(i + 1);
        }

    });

    var cloned = jQuery('.slick-cloned').length
    var slides = jQuery('.slick-slide').length

    var totalSlides = slides - cloned;
    jQuery('.slick-dots li:last-child').after('<li class="total"><span>' + totalSlides + '</span></li>')
});